/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

/**
 *
 * @author Room107
 */
public enum Airport {
    BKK("Suvarnabhumi Airport", "Bangkok", "TH"),
    DMK("Don Mueang International Airport", "Bangkok", "TH"),
    HKT("Phuket International Airport", "Phuket", "TH"),
    CNX("Chiang Mai International Airport", "Chiang Mai", "TH"),
    HDY("Hat Yai International Airport", "Songkhla", "TH"),
    CEI("Mae Fah Luang Chiang Rai International Airport", "Chiang Rai", "TH");
    
    Airport(String airportName, String city, String country){
        this.airportName = airportName;
        this.city = city;
        this.country = country;
    }
    
    private final String city;
    private final String country;
    private final String airportName;

    public String city(){
        return city;
    }
    public String country(){
        return country;
    }   
    public String airportName(){
        return airportName;
    }
}
